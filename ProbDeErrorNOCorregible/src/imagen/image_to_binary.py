from PIL import Image
import matplotlib.pyplot as plt
import numpy as np

def image_to_binary(image_rgb, x, y):

    vT = []
    bkT = []
    bfT = []
    bits = []


    # Funcion codificadora

    for i in range(0, y):
        vT.append([])
        bkT.append([])
        for j in range(0, x):
            rgb_pixel_value = image_rgb.getpixel((j,i))

            # Cada muestra v(k) se almacena en vT.
            vT[i].append(rgb_pixel_value)

            # Se asigna cada valor de v(k) codificado, a bkT
            rgb_to_binary = map(bin, image_rgb.getpixel((j,i)))
            bkT[i].append(rgb_to_binary)

            # Se extiende al cadena de codificacion, agregando el valor a bfT
            bfT.extend(rgb_to_binary)
        

    cnt_removeZero = 0

    # Recorre todos los elementos (valores R, G y B) de la lista

    for i in range(0, len(bfT)):
        # Obtiene el tamano del elemento 
        numBits = bfT[i].count("0") + bfT[i].count("1") + 1
        # Se recorre el elemento 
        for j in range(0, numBits):
            # Mientras no sea b, entonces...
            if(bfT[i][j] != 'b'):
                # Ingrese el bit en la lista bits
                bits.append(bfT[i][j])
        # Extension de signo     
        if(numBits < 10):
            # Recorre y agrega al elemento los bits restantes
            for k in range(numBits, 10):
                # Ingresa ceros desde la posicion final
                bits.insert(cnt_removeZero, "0")
        # Se elimina el cero sobrante
        bits.pop(cnt_removeZero)
        # Cuenta un nuevo elemento
        cnt_removeZero = cnt_removeZero + 8


    # Escribir el archivo .bin
    j = 0
    mi_archivo = open("prueba.bin", "w")

    for i in range(0, len(bits)):
        if(j == 8):
            mi_archivo.write("\n")
            j = 0
        mi_archivo.write(bits[i])
        j = j + 1

    mi_archivo.close()


def main():
    image = Image.open("red.jpg")
    image_rgb = image.convert("RGB")
    x = image.size[0]		#columnas
    y = image.size[1]       #filas
    image_to_binary(image_rgb, x, y)

main()