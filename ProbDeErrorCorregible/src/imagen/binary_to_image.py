from PIL import Image
import matplotlib.pyplot as plt
import numpy as np

def binary_to_image(x, y):

    ###################Se crea el archivo####################
    mi_archivo = open("imagen.txt", "r")
    rx = mi_archivo.readlines()
    #########################################################

    ##################Se quitan los \n#######################
    bfR = []
    e = 0
    vR = []
    for i in range(0, len(rx)):
        if rx[i] != "xxxxxxxx\n":
            bfR.append(rx[i].replace("\n",""))        
    #########################################################

    ############Se reconstruyen los pixels a RGB#############
    for i in range(0, y):
        vR.append([])
        for j in range(0, x):
            rgb_to_decimal = []
            rgb_to_decimal.append(int(str(bfR[e]), 2))
            rgb_to_decimal.append(int(str(bfR[e+1]), 2))
            rgb_to_decimal.append(int(str(bfR[e+2]), 2))
            vR[i].append(rgb_to_decimal)
            e = e + 3	
    #########################################################
    mi_archivo.close()
    return vR

def main():
    image = Image.open("paisaje.png")
    image_rgb = image.convert("RGB")
    x = image.size[0]		#columnas
    y = image.size[1]       #filas
    plt.imshow(np.array(binary_to_image(x, y)), interpolation='none')
    plt.show()
    
main()