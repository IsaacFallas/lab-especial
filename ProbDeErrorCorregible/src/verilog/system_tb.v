`timescale 1 ns / 1 ps

module system_tb;

	reg clk = 1;
	always #5 clk = ~clk;
	reg resetn = 0;
	
	wire trap;
	wire [15:0] onBits;
	wire [15:0] corruptedBits;
	wire [7:0] inCoder, outDecoder;
	wire zero;	
	integer probability, errorBit;
    reg [15:0] error;
    wire fin;
    wire [19:0] cnt;
	
	integer log;
	
	initial begin
	
		if ($test$plusargs("vcd")) begin
			$dumpfile("system.vcd");
			$dumpvars(0, system_tb);
		end
		
		log = $fopen("/home/fabian/Documentos/LabDigitales/LabEspecial/src/imagen/imagen.txt","w");
		
		repeat (100) @(posedge clk);
		resetn <= 1;
	end
	
	always @(ready_out) begin
	    if(ready_out && resetn)
		  $fwrite(log, "%b\n", outDecoder);		
	end

	assign corruptedBits = onBits^error;
	
	wire ready_out;

	system uut (
		    // Outputs
		    .trap		(trap),
		    .onBits	(onBits),
			.zero	(zero),
			.cnt	(cnt),
			.inCoder (inCoder),
			.outDecoder (outDecoder),
			.fin	(fin),
			.ready_out   (ready_out),
		    // Inputs
		    .clk		(clk),
		    .resetn		(resetn),
			.corruptedBits		(corruptedBits));

	reg [7:0] checkbuffer [3:0]; 
	reg [1:0] readCount = 2'b0;
	reg [1:0] writeCount = 2'b0;
	reg [17:0] totalErrors = 18'b0;
	reg errorCheck;

	always @(posedge clk) begin
		/*if (resetn && out_byte_en) begin
			$write("%c", out_byte);
			$fflush;
		end*/
		if (fin && cnt == 201603) begin
		    $fclose(log);
			$finish;
		end
	end

    always @(onBits) begin
        probability = $urandom%100;
        if (probability<7) begin
            errorBit = $urandom%15;
            error = 1<<errorBit;
        end else error = 16'b0;
    end

	always @(inCoder) begin
		checkbuffer[writeCount] = inCoder;
		writeCount = writeCount+1;
	end

	always @(outDecoder) begin
		if (checkbuffer[readCount] != outDecoder) begin
			errorCheck = 1;
			totalErrors = totalErrors+1;
		end else errorCheck = 0;

		readCount = readCount+1;
		
	end


endmodule
