`timescale 1 ns / 1 ps


module decodificador(
    input clk,
    input resetn,
    input [15:0] v,
    output reg [7:0] m,
    output reg ready,
    output reg fin
    );
//Registros para uso de la máquina de estados
reg [1:0] state, nextstate;
//Estados de la máquina de estados
parameter transformacion = 0, deteccion = 1, correccion = 2;
//Matrices
wire [7:0] H [15:0];
wire [7:0] M [15:0];
wire [3:0] p [3:0];
//Vectores
reg [7:0] s;
reg [15:0] e;
reg [15:0] u;
reg [15:0] V;
//assigns de las matrices

//matriz H
assign H [0] =  8'b10000000;
assign H [1] =  8'b01000000;
assign H [2] =  8'b00100000;
assign H [3] =  8'b00010000;
assign H [4] =  8'b00001000;
assign H [5] =  8'b00000100;
assign H [6] =  8'b00000010;
assign H [7] =  8'b00000001;
assign H [8] =  8'b00011000;
assign H [9] =  8'b00111110;
assign H [10] = 8'b10010000;
assign H [11] = 8'b11111111;
assign H [12] = 8'b01011010;
assign H [13] = 8'b00011110;
assign H [14] = 8'b10100000;
assign H [15] = 8'b01000100;

//matriz M
assign M [0] = 8'b10000000;
assign M [1] = 8'b01000000;
assign M [2] = 8'b00100000;
assign M [3] = 8'b00010000;
assign M [4] = 8'b00001000;
assign M [5] = 8'b00000100;
assign M [6] = 8'b00000010;
assign M [7] = 8'b00000001;
assign M [8] = 8'b00000000;
assign M [9] = 8'b00000000;
assign M [10] = 8'b00000000;
assign M [11] = 8'b00000000;
assign M [12] = 8'b00000000;
assign M [13] = 8'b00000000;
assign M [14] = 8'b00000000;
assign M [15] = 8'b00000000;

//Contador de errores corregidos
reg [13:0] cnt_bugs_fixed;
reg error_fixed;
//Máquina de estados
integer i, j;
always @(posedge clk) begin
    if (!resetn) begin
        state <= transformacion;
        cnt_bugs_fixed <= 0;
    end else begin
        if (error_fixed) begin
            cnt_bugs_fixed <= cnt_bugs_fixed + 1;
        end
        state <= nextstate;
    end
end

always @(state or v) begin
    case (state)
        transformacion: begin
            error_fixed = 0;
            for (i = 7; i >= 0; i = i - 1) begin
                s[i] = 0;
                for (j = 0; j < 16; j = j + 1) begin
                    s[i] = H[j][i] & v[15-j] ^ s[i];
                end
            end
            V = v;
            ready = 1;
            fin = 0;
            nextstate = deteccion; 
        end
        deteccion: begin
            for (i = 0; i < 16; i = i + 1) begin
                if (H[i] == s) begin
                    e[15-i] = 1;
                    error_fixed = 1;
                end else
                    e[15-i] = 0;
            end
            u = V ^ e;
            ready = 0;
            fin = 0;
            nextstate = correccion;
        end
        correccion: begin
            error_fixed = 0;
            for (i = 7; i >= 0; i = i - 1) begin
                m[i] = 0;
                for (j = 0; j < 16; j = j + 1) begin
                    m[i] = M[j] & u[i] ^ u[i];
                end
            end
            ready = 0;
            fin = 1;
            nextstate = transformacion;
        end
        default: nextstate = transformacion;
    endcase
end
endmodule

module CC(
    input clk,
    input resetn,
    input ready,
    input [7:0] ikBits,
    output reg [15:0] onBits
);

    wire [15:0] matrixG [7:0];  // matrix generadora
    reg [15:0] codifiedBits;
    reg multResult;
    integer i;
    integer j;

    assign matrixG[0] = 16'h1880;
    assign matrixG[1] = 15936;
    assign matrixG[2] = 36896;
    assign matrixG[3] = 65296;
    assign matrixG[4] = 23048;
    assign matrixG[5] = 7684;
    assign matrixG[6] = 40962;
    assign matrixG[7] = 17409;

    always @(posedge clk) begin

        if (!resetn) begin

            //codifiedBits <= 0;
            //onBits <= 0;
            
        end else if (resetn && ready) begin

            onBits <= codifiedBits;
            
        end else if (resetn && !ready) begin
            
            onBits <= onBits;

        end 
        
    end

    always @(ikBits) begin

        for (i = 0; i < 16; i = i+1) begin

            multResult = 0;
                
                for (j = 0; j < 8; j = j+1) begin
                    
                    multResult = (matrixG[j][i] & ikBits[7-j]) ^ multResult;
                
                end

            codifiedBits[i] = multResult;

        end
        
    end

endmodule // CC

module system (
	input            clk,
	input            resetn,
    input [15:0]    corruptedBits,
	output [15:0]    onBits,		
	output           trap,
	output reg 			zero,
	output [7:0] result,
	output [19:0] cnt,
    output [7:0] inCoder,
    output [7:0] outDecoder,
	output fin,
	output ready_out
);

	// 4096 32bit words = 16kB memory
	parameter MEM_SIZE = 201600;

	reg [7:0] ikBits;
    wire ready;
    wire [7:0] m;
    wire [15:0] v;
    wire error;
    wire [7:0] inCoder;
    wire [7:0] outDecoder; 
    
    assign cnt = counterMem;
    assign inCoder = ikBits;
    assign outDecoder = m;

    assign v = corruptedBits;
    
    assign ready_out = ready;

	CC codCanal(/*AUTOINST*/
		    // Outputs
		    .onBits		(onBits[15:0]),
		    // Inputs
		    .clk		(clk),
		    .resetn		(resetn),
		    .ready		(ready),
		    .ikBits		(ikBits[7:0]));

    decodificador decodificador(
				// Outputs
				.m		(m[7:0]),
				.ready		(ready),
				.fin    (fin),
				// Inputs
				.clk		(clk),
				.resetn		(resetn),
				.v		(v[15:0]));

	reg [7:0] memory [0:MEM_SIZE-1];

`ifdef SYNTHESIS
    initial $readmemb("../imagen/imagen.bin", memory);
`else
	initial $readmemb("imagen.bin", memory);
`endif

	reg [19:0] counterMem;

	always @(posedge clk) begin

		if (!resetn) begin
			counterMem <= 0;
		end else if (resetn && ready) begin
			counterMem <= counterMem+1;
			ikBits <= memory[counterMem];
        end else if (resetn && !ready) begin
            counterMem <= counterMem;
			ikBits <= ikBits;
        end
		
	end


endmodule
