module decodificador(
    input clk,
    input resetn,
    input [15:0] v,
    output reg [7:0] m,
    output reg ready,
    output reg fin
    );
//Registros para uso de la máquina de estados
reg [1:0] state, nextstate;
//Estados de la máquina de estados
parameter transformacion = 0, deteccion = 1, correccion = 2;
//Matrices
wire [7:0] H [15:0];
wire [7:0] M [15:0];
wire [3:0] p [3:0];
//Vectores
reg [7:0] s;
reg [15:0] e;
reg [15:0] u;
reg [15:0] V;
//assigns de las matrices

//matriz H
assign H [0] =  8'b10000000;
assign H [1] =  8'b01000000;
assign H [2] =  8'b00100000;
assign H [3] =  8'b00010000;
assign H [4] =  8'b00001000;
assign H [5] =  8'b00000100;
assign H [6] =  8'b00000010;
assign H [7] =  8'b00000001;
assign H [8] =  8'b00011000;
assign H [9] =  8'b00111110;
assign H [10] = 8'b10010000;
assign H [11] = 8'b11111111;
assign H [12] = 8'b01011010;
assign H [13] = 8'b00011110;
assign H [14] = 8'b10100000;
assign H [15] = 8'b01000100;


//matriz M
assign M [0] = 8'b10000000;
assign M [1] = 8'b01000000;
assign M [2] = 8'b00100000;
assign M [3] = 8'b00010000;
assign M [4] = 8'b00001000;
assign M [5] = 8'b00000100;
assign M [6] = 8'b00000010;
assign M [7] = 8'b00000001;
assign M [8] = 8'b00000000;
assign M [9] = 8'b00000000;
assign M [10] = 8'b00000000;
assign M [11] = 8'b00000000;
assign M [12] = 8'b00000000;
assign M [13] = 8'b00000000;
assign M [14] = 8'b00000000;
assign M [15] = 8'b00000000;
//Máquina de estados
integer i, j;
always @(posedge clk) begin
    if (!resetn) begin
        state <= transformacion;
    end else begin
        state <= nextstate;
    end
end

always @(state or v) begin
    case (state)
        transformacion: begin
            for (i = 7; i >= 0; i = i - 1) begin
                s[i] = 0;
                for (j = 0; j < 16; j = j + 1) begin
                    s[i] = H[j][i] & v[15-j] ^ s[i];
                end
            end
            V = v;
            ready = 1;
            fin = 0;
            nextstate = deteccion; 
        end
        deteccion: begin
            for (i = 0; i < 16; i = i + 1) begin
                if (H[i] == s)
                    e[15-i] = 1;
                else
                    e[15-i] = 0;
            end
            u = V ^ e;
            ready = 0;
            fin = 0;
            nextstate = correccion;
        end
        correccion: begin
            for (i = 7; i >= 0; i = i - 1) begin
                m[i] = 0;
                for (j = 0; j < 16; j = j + 1) begin
                    m[i] = M[j] & u[i] ^ u[i];
                end
            end
            ready = 0;
            fin = 1;
            nextstate = transformacion;
        end
        default: nextstate = transformacion;
    endcase
end
endmodule